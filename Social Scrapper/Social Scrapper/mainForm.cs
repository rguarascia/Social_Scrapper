﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Facebook;
using Tweetinvi;
using InstaSharp;

//Ryan Guarascia
//Start: 25/09/2014
//Should scrap selected social networking websites and return their post and other important information.
//Credits go to my Tech teacher for giving me the idea.

namespace Social_Scrapper
{
    public partial class mainForm : Form
    {

        Scrapper scrapper = new Scrapper();

        public mainForm()
        {
            InitializeComponent();
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            cmbSites.DataSource = new string[] { "Twitter", "Facebook", "Instagram", "All" };
            lstTwitter.DataSource = scrapper.twitterUserSearch("rynoh97");
        }
    }
}
