﻿namespace Social_Scrapper
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstTwitter = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.listFacebook = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lstInstagram = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUsername2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbSites = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lstTwitter
            // 
            this.lstTwitter.FormattingEnabled = true;
            this.lstTwitter.Location = new System.Drawing.Point(12, 114);
            this.lstTwitter.Name = "lstTwitter";
            this.lstTwitter.Size = new System.Drawing.Size(296, 537);
            this.lstTwitter.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(20, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(288, 23);
            this.label4.TabIndex = 8;
            this.label4.Text = "Twitter";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listFacebook
            // 
            this.listFacebook.FormattingEnabled = true;
            this.listFacebook.Location = new System.Drawing.Point(314, 114);
            this.listFacebook.Name = "listFacebook";
            this.listFacebook.Size = new System.Drawing.Size(296, 537);
            this.listFacebook.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(322, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 23);
            this.label1.TabIndex = 10;
            this.label1.Text = "Facebook";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstInstagram
            // 
            this.lstInstagram.FormattingEnabled = true;
            this.lstInstagram.Location = new System.Drawing.Point(616, 114);
            this.lstInstagram.Name = "lstInstagram";
            this.lstInstagram.Size = new System.Drawing.Size(296, 537);
            this.lstInstagram.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(624, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(288, 23);
            this.label2.TabIndex = 12;
            this.label2.Text = "Instagram";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Real Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(78, 9);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(230, 20);
            this.txtName.TabIndex = 15;
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(380, 9);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(230, 20);
            this.txtUsername.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(314, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Username";
            // 
            // txtUsername2
            // 
            this.txtUsername2.Location = new System.Drawing.Point(682, 9);
            this.txtUsername2.Name = "txtUsername2";
            this.txtUsername2.Size = new System.Drawing.Size(230, 20);
            this.txtUsername2.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(616, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Username 2";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(15, 35);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(120, 23);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "Commit Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(15, 62);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 21;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(837, 35);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 22;
            this.btnSave.Text = "Save from ...";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // cmbSites
            // 
            this.cmbSites.FormattingEnabled = true;
            this.cmbSites.Location = new System.Drawing.Point(791, 62);
            this.cmbSites.Name = "cmbSites";
            this.cmbSites.Size = new System.Drawing.Size(121, 21);
            this.cmbSites.TabIndex = 23;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 663);
            this.Controls.Add(this.cmbSites);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtUsername2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lstInstagram);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listFacebook);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstTwitter);
            this.Controls.Add(this.label4);
            this.Name = "mainForm";
            this.Text = "Social Scrapper";
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstTwitter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listFacebook;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lstInstagram;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUsername2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbSites;
    }
}

